#include <iostream>
#include <string>
#include <Poco/Base64Encoder.h>
#include <Poco/Base64Decoder.h>
#include <fstream>
#include <sstream>
using namespace std;

class Base64_decode_encode{
public:
    Base64_decode_encode();
    ~Base64_decode_encode();

    std::string encode(std::string str);
    std::string decode(std::string str);
private:
    Poco::Base64Decoder* decoder;
    Poco::Base64Encoder* encoder;
};

Base64_decode_encode::Base64_decode_encode()
{
    decoder = nullptr;
    encoder = nullptr;
}

Base64_decode_encode::~Base64_decode_encode()
{
    if (decoder != nullptr) delete decoder;
    if (encoder != nullptr) delete encoder;
}

std::string Base64_decode_encode::encode(std::string str)
{
    ostringstream stream;
    encoder = new Poco::Base64Encoder(stream);
    *encoder << str;
    encoder->close();
    std::string ret = stream.str();
    delete encoder;
    encoder = nullptr;
    return ret;

}

std::string Base64_decode_encode::decode(std::string str)
{
    istringstream stream(str);
    decoder = new Poco::Base64Decoder(stream);
    std::string ret;
    *decoder >> ret;
    delete decoder;
    decoder = nullptr;
    return ret;
}



int main(int argc, char** argv)
{
    Base64_decode_encode endecoder;
    std::string str = "hello.everyone";
    std::string ret = endecoder.encode(str);
    std::string de = endecoder.decode(ret);
    std::cout << "encode: " << ret  << endl << "decode " << de  << std::endl;
    return 0;
}