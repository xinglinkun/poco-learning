#include <iostream>
#include <fstream>
#include <Poco/MD5Engine.h>
#include <Poco/DigestStream.h>

using namespace std;



class myMD5{
public:
    myMD5();
    ~myMD5();
    std::string addfile(std::string path);
    std::string addstring(std::string string);

    Poco::MD5Engine m_md5;
    Poco::DigestOutputStream* m_dos;
};


myMD5::myMD5()
{
    m_dos = nullptr;
    m_dos = new Poco::DigestOutputStream(m_md5);
}
myMD5::~myMD5()
{
    if (m_dos != nullptr) delete m_dos;
    m_dos = nullptr;
}
std::string myMD5::addfile(std::string path)
{
    ifstream file(path);
    if (!file.is_open()) return "file is open error";
    std::string str;
    while (getline(file, str))
    {
       *m_dos << str;
       *m_dos << endl;
    }
    file.close();
    std::string ret =  Poco::DigestEngine::digestToHex(m_md5.digest());
    m_dos->clear();
    return ret;
    
}

std::string myMD5::addstring(std::string string)
{
    *m_dos << string;
    std::string ret =  Poco::DigestEngine::digestToHex(m_md5.digest());
    m_dos->clear();
    return ret;
}
int main(int argc, char** argv)
{

    myMD5 md5;
    std::string str = md5.addfile("/home/xiaoc/test.txt");
    std::string str2= md5.addfile("/home/xiaoc/code.deb");
    std::string str3 = md5.addstring("xinglinkun");
    cout << str << endl;
    cout << str2 << endl;
    cout << str3 << endl;

    return 0;
}